# Build the Search Engine
In this section, you will perform and experiment with indexing and retrieval on a special dataset called the MOOCs dataset.

## Exploring the Dataset
The MOOCs dataset contains the descriptions found on the webpages of around 23,000 MOOCs (Massive Open Online Courses). 
You will start by exploring the dataset. 
Navigate to Assignment_1/moocs. 
This directory contains the files that describe the dataset.

- `moocs.dat` contains the content of the webpages of all the MOOCs; each MOOC's main page occupies exactly one line in the file.
Feel free to open the file to get an idea about the contents, but be wary that it is a large file and might take some time to load.

- `moocs.dat.names` contains the names and the URLs of the MOOCs.
The entry on line x in moocs.dat.names corresponds to the MOOC on line x in moocs.dat.
moocs-queries.txt contains a set of queries that you will use to evaluate the effectiveness of your search engine.

- `moocs-qrels.txt` contains the relevance judgments corresponding to the queries in moocs-queries.txt.
Each line in moocs-qrels.txt has the following format: (querynum documentID 1).
This means that the document represented by documentID is a relevant document for the query whose number is querynum. 
The relevance judgments in moocs-qrels.txt were created by human assessors who ran the queries and chose the relevant documents.
Later on in the assignment, you are going to use these judgments to quantify the performance of your search engine.

## Indexing

Now that you have an idea about tokenization, text filtering, and the dataset to be used, you will proceed to index the MOOCs dataset. In this process, an inverted index will be created. 

As you have seen in the lectures, the inverted index is a data structure that supports efficient retrieval of documents by allowing the lookup of all the documents that contain a specific term.

Before you proceed to index the dataset, we encourage you to open config.toml and examine the settings that we have already configured. 
For instance, the snippet shown below tells the indexer where to find the MOOCs dataset, specifies that it is a line corpus (i.e., each document is on one line), and defines the name of the inverted index to be created.
The forward index will not be used in this assignment.

```sh
query-judgements = "../../meta/data/moocs/moocs-qrels.txt" 
querypath = "../../meta/data/moocs/" 
corpus = "line.toml" 
dataset = "moocs" 
forward-index = "moocs-fwd" 
inverted-index = "moocs-inv" 
```

You should also have a look at another important snippet:

```sh
[[analyzers]]
method = "ngram-word"
ngram = 1
filter = "default-unigram-chain"
```
The settings under the analyzers tag control how tokenization and filtering are performed, prior to creating the inverted index. 
The tokenizer will segment each document into unigrams. MeTA uses its default filtering chain, which performs a couple of predefined filters including lower case conversion, length filtering (which discards tokens whose length is not within a certain range), and stemming. 
To read more about modifying META's default tokenization and filtering behavior see MeTA’s analyzers and filters page.

To index the dataset, in the `Assignment_1/build`directory run:
```sh
../../meta/build/index ../config.toml
```
(index program is in the `meta/build` directory under root). 
You should see something like below:

```sh
$ ./meta/build/index ../config.toml
1463154868: [info]     Loading index from disk: moocs-inv (root/meta/src/index/inverted_index.cpp:178)
Number of documents: 23566
Avg Doc Length: 383.014
Unique Terms: 193614
Index generation took: 0.007 seconds
```

This will start by performing tokenization and applying the text filters defined in `config.tom`; it then creates the inverted index and places it in`/meta/build/moocs-inv`. 
When the program finishes execution, you should get a summary of the indexed corpus as above

## Searching
After creating the inverted index, you can efficiently search the MOOCs dataset. 
The ranking function to be used in retrieving documents is defined in `config.toml`. 
Open `config.toml` and look for the ranker tag. Under this tag, you will see that the default ranker is "bm25" along with values assigned to its three parameters. To test your search engine, you can use the `interactive-search` program provided with MeTA:
```sh
cd Assignment_1/build
../../meta/build/interactive-search ../config.toml
```

Enter any query you want, and the top results should show up instantaneously. Feel free to experiment with different queries to explore the contents of the dataset.

When you finish, enter a blank query to exit.

In what follows you will evaluate the performance of the search engine using different ranking functions and parameters.
The main evaluation measure to be used is MAP (mean average precision), which is the arithmetic mean of the average precision of all the queries being used for evaluation.
We have provided you with a program called `ranking-experiment` which evaluates the MAP of any retrieval function you specify in `config.toml`.

### Task 4: BM25 (5 pts)
Evaluate the performance of bm25 with default parameters (i.e., do not change `config.toml`) by running:
```sh
cd Assignment_1/build
./ranking-experiment ../config.toml task4
```

You should see a list of the top results corresponding to the different test queries, the precision at 10 for each query, and the final MAP value.

At the same time, we create a output file for you to submit: `/Assignment_1/build/Assignment1/task4.txt`

Submit `task4.txt`.

### Task 5: BM25 with tuned parameters (10 pts)
Change the parameter `b` of bm25 in `config.toml` to 0.75

```sh
ranker] 
method = "bm25" 
k1 = 1.2 
b = 0.75 
k3 = 500
```
and run again:
```sh
cd Assignment_1/build
./ranking-experiment ../config.toml task5
```
and submit your `/Assignment_1/build/Assignment1/task5.txt`

### Task 6: PL2 (30 pts)
MeTA has several built-in rankers other than bm25; see MeTA’s Index Reference page and the directory meta/src/index/ranker for the list of built-in rankers. 
In this task you will implement a well known retrieval function called PL2, which is not available in MeTA. 
PL2 is a member of the Divergence from Randomness framework, which is based on the idea that the more different the within-document term frequency is from the term frequency generated by a random process, the higher the information content of the term is. 
PL2's scoring function is given by:

## photo

and `tf` is the term frequency in the document, `Avgl` is the average document length in the whole corpus, `Dl` is the document length, and λ and c are positive tunable parameters.

Every ranking function in MeTA should have its own class that subclasses the base class `ranker`.

Go to [MeTA’s ranker Class Reference](https://meta-toolkit.github.io/meta/doxygen/classmeta_1_1index_1_1ranker.html) and have a quick look at the member functions of ranker. 

One important function in `ranker` you should be aware of is:

```c
float score_one(const index::score_data&) override;
```
which calculates the ranking score of one matched term. That is, if the scoring function is written as

### photo2

where t is a matched term and w(t,Q,D) is the score of that term, then `score_one = w(t,Q,D)`. 

After each call to `score_one`, the result will be added to the document's accumulator, which is responsible for returning the final document score.

The struct of type `score_data` passed as a parameter to `score_one` contains important statistics about the matched term (such as the number of occurrences in the document and the document length) and other global statistics like the average document length in the corpus. 
For more information on the members of this struct, check [MeTA’s score_data Reference](https://meta-toolkit.github.io/meta/doxygen/structmeta_1_1index_1_1score__data.html).

Now that you have a basic understanding of the base class ranker and the function `score_one`, you should be able to implement the PL2 ranking function.
Go to `meta/src/index/tools/` and 
open `ranking-experiment.cpp` in a text editor or IDE.

 We have already provided a code that creates a new ranker class called "pl2_ranker." 
 As you can see, "pl2_ranker" is a derived class from the base class `ranker`. Examine the members of "pl2_ranker" and the comments next to them.

Your task is to complete the implementation of the function:

```c
float score_one(const index::score_data&)
```
in order to return the score defined by PL2. Make sure to follow the instructions provided as comments in the body of the function.

In addition, in config.toml, change the ranker to "pl2" with appropriate parameters. In below, we use "#" to comment out lines that we don't need.

```sh
[ranker]
#method = "bm25"
#k1 = 1.2
#b = 0.75
#k3 = 500
method = "pl2"
c = 7
lambda = 0.1
```
Now, you need to recompile the code again:
```sh
cd Assignment_1/build
cmake .. -DCMAKE_BUILD_TYPE=Release; make -j8
./ranking-experiment ../config.toml task6
```

And again, submit your results `/Assignment_1/build/Assignment1/task6.txt`

### Task 7: Tuning PL2 (20 pts)
Note: If you are a Mac user, you will need to use the GNU utils (e.g., gnu sed) instead of BSD to get the results.

In this task you will tune the PL2 ranking function in order to get better values of c and λ. 

Open `ranking-experiment.cpp`, and look for the function “pl2_tune.” This function is expected to sweep over pairs of the parameters c and λ and select the pair that gives the highest MAP. 

However, the function has a few incomplete lines:

```sh
maxmap = 0; // Change 0 to the correct value
cmax = 0; // Change 0 to the correct value
lambdamax = 0; // Change 0 to the correct value
```
Read the comments beside the function's code and try to understand how it works. After that, change the values in the incomplete lines.

You should now recompile MeTA and run `ranking-experiment`:

```sh
cd Assignment_1/build
cmake .. -DCMAKE_BUILD_TYPE=Release; make -j8
./ranking-experiment ../config.toml task7
```

`ranking-experiment` will then return the maximum MAP along with the optimal parameters λ and c that achieve this maximum. 
You should see a noticeable improvement in the MAP over what you got in Task 6.

And again, submit your results `/Assignment_1/build/Assignment1/task7.txt`

After you finish Task 7, open `config.toml` and revert to bm25 as the default ranker.

You can do this by removing the hash symbols preceding bm25 and its parameters and adding hash symbols to pl2 and its parameters.

### Task 8: Relevance Judgments (20 pts)
The next programming assignment will be a competition where your search engine will be evaluated based on relevance judgments made by all the students in this course. 
Relevance judgments are query-document pairs marked as relevant or irrelevant by users. 

We expect to collect a large and diverse pool of judgments, which should allow a fair evaluation of your search engines in the second assignment.

For this purpose, you are required to come up with a query about a topic you would like to study in an online course. 

The query can be something like "information retrieval." 

You should also write a brief description of what you expect to learn from the course you are searching for. 

For example, one description is "To learn how to efficiently do retrieval from large datasets." 
In the terminal, execute:

```sh
cd Assignment_1/build
./relevance-judgements ../config.toml
```

You should perform the following steps after running relevance-judgements:

1. Enter the query of your choice.

2. Enter a description of what you are expecting to learn. You will be shown a list of the top 20 MOOCs corresponding to your query.

3. Enter the numbers of the relevant MOOCs separated by spaces.

In case you could not find any relevant documents for your query, try another one. 

Also, feel free to submit more than one query as this should help more in the competition.

The relevance judgments you came up with are saved in a file called task8.txt. You can take a look at its content, but do not modify it.

Currently the grade cannot grade anything that has multiple lines and please remove any "return line" ('\n') from task8.txt for submission purpose. You do not add any new whitespaces.

Submit your `/Assignment_1/build/Assignment1/task8.txt`
Submit the file `docs.stops.txt.wc` to partTask 1. 

In the My submission tab, click Createsubmission. 

Then click on Task 1: Stopword Removal and upload your file.